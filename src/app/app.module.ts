import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, } from '@angular/router';
import { AppComponent } from './app.component';
import { PreguntaUnoComponent } from './pregunta-uno/pregunta-uno.component';
import { PreguntaDosComponent } from './pregunta-dos/pregunta-dos.component';
import { TablaArrayComponent } from './pregunta-uno/tabla-array/tabla-array.component';
import { TablaDictComponent } from './pregunta-dos/tabla-dict/tabla-dict.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LogInterceptorService } from './services/log-interceptor.service';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from './home/home.component';
import { AlertComponent } from './alert/alert.component';

@NgModule({
  declarations: [
    AppComponent,
    PreguntaUnoComponent,
    PreguntaDosComponent,
    TablaArrayComponent,
    TablaDictComponent,
    NavBarComponent,
    HomeComponent,
    AlertComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([
   
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'preguntauno', component: PreguntaUnoComponent, pathMatch: 'full' },
      { path: 'preguntados', component: PreguntaDosComponent, pathMatch: 'full' },

    //  { path: 'pacientes', component: PacientesComponent, canActivate: [AuthGuardService] },
   //   { path: 'pacientes-agregar', component: PacientesFormComponent, canActivate: [AuthGuardService] },
    //  { path: 'pacientes-editar/:id', component: PacientesFormComponent, canActivate: [AuthGuardService] },
   //   { path: 'pacientes-detalle/:id', component: PacientesDetalleComponent, canActivate: [AuthGuardService] },

  ]),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LogInterceptorService,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
