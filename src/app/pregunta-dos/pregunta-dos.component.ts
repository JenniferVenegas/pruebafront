import { Component, OnInit, ViewChild ,ElementRef} from '@angular/core';
import { IDiccionario } from './diccionario';
import { PreguntasService} from  '../services/preguntas.service';

@Component({
  selector: 'app-pregunta-dos',
  templateUrl: './pregunta-dos.component.html',
  styleUrls: ['./pregunta-dos.component.css']
})
export class PreguntaDosComponent implements OnInit {
  @ViewChild('spinnerCarga', { static: false }) spinnerCarga: ElementRef;
  @ViewChild('botonCargar', { static: false }) botonCargar: ElementRef;

  listaDiccionario : Array<IDiccionario>;
  retornoapi: any;
  tipoAlerta: number =0;
  childAlert = 0;

  constructor( private preguntasService: PreguntasService) { }

  ngOnInit(): void {
    
  }

  resetAlert(number: any):void {
    this.tipoAlerta =0;
  } 

  obtenerDatos(){
    this.activarButtonCarga();

    //consumimos servicio y actualizamos variable "retornoapi"
    this.preguntasService.obtenerDict()
    .subscribe(DictDesdeApi=> {
                              this.retornoapi = DictDesdeApi;
                              },
              error => this.mostrarError(),
              () => this.AnalizarCadenas()); 
 }
  

 AnalizarCadenas(){
  this.resetButton();
  //se almacena retorno de servicio como arreglo de IDiccionario[]
  var diccionario: IDiccionario[] = JSON.parse(this.retornoapi['data']);
  let letras = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  
  if( diccionario.length >0 ){
  // si existe "data" inicializo valores no contenidos en el Json y posteriormente hago un match con cada letra
  // del arreglo "letras" y la cadena "paragraph" de cada IDicionnario en el arreglo. Guardo el total de c/letra presente en paragraph en el arreglo "detalleLetras"
  diccionario.forEach(function(fila, index) { 
                            fila.detalleLetras =[];
                            letras.forEach( function(letra , ind) {
                                  diccionario[index].detalleLetras[ind]= (fila.paragraph.match(new RegExp(letra, "gi"))|| []).length;
                                  // luego de buscar las letras, busco los números. Si encuentra números los suma y asocia a "sumatoria" de "IDiccionario"
                                  var numero = fila.paragraph.match(/\d/g);
                                  let total =0;
                                  if(numero){
                                    numero.forEach(function(num) {
                                    total = total + +num;}
                                  );
                                  }
                             diccionario[index].sumatoria = total;
                            });                    
  });  
  }
  // actualizo variable que se enviará como Input a componente tabla-dict
    this.listaDiccionario = diccionario;
 }

 
mostrarError(){
  this.resetButton()
  this.tipoAlerta=2;
}

mostrarWarning(){
  this.resetButton()
  this.tipoAlerta=1;
}

resetButton(){
    this.spinnerCarga.nativeElement.hidden=true;
    this.botonCargar.nativeElement.innerText = "Obtener datos diccionario";
    this.botonCargar.nativeElement.disabled = false;
}

activarButtonCarga(){
  this.spinnerCarga.nativeElement.hidden = false;
  this.botonCargar.nativeElement.innerText = "Cargando ..."
  this.botonCargar.nativeElement.disabled = true;
}
}
