import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaDictComponent } from './tabla-dict.component';

describe('TablaDictComponent', () => {
  let component: TablaDictComponent;
  let fixture: ComponentFixture<TablaDictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaDictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaDictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
