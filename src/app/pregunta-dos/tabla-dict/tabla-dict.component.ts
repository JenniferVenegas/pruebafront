import { Component, OnInit, Input } from '@angular/core';
import { IDiccionario } from '../diccionario';

@Component({
  selector: 'app-tabla-dict',
  templateUrl: './tabla-dict.component.html',
  styleUrls: ['./tabla-dict.component.css']
})
export class TablaDictComponent implements OnInit {

  @Input()   listaDiccionario : Array<IDiccionario>;
  nombreColumnas = ['N\°','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',"Suma numericos"];
  constructor() { }

  ngOnInit(): void {
  }

}
