
export interface IDiccionario {
    paragraph: string;
    number: number;
    hasCopyright: number;
    detalleLetras : number[];
    sumatoria: number;
  }
  