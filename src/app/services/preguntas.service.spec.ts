import { TestBed } from '@angular/core/testing';

import { PreguntaUnoService } from './pregunta-uno.service';

describe('PreguntaUnoService', () => {
  let service: PreguntaUnoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PreguntaUnoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
