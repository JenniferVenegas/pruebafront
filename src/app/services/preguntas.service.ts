import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PreguntasService {

  private apiURL = "http://patovega.com/prueba_frontend/";
   
  constructor(private http: HttpClient) { }

  

  obtenerArray(): Observable<any> {
    return this.http.get<any>(this.apiURL+"array.php");
  }


  obtenerDict(): Observable<any> {
    return this.http.get<any>(this.apiURL+"dict.php");
  }
}
