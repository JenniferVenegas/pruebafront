import { Component, OnInit , Input, Output, OnChanges, SimpleChanges, ViewChild, ElementRef, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnChanges {

  @Input() tipoAlerta: number;
  @Output() childAlert: EventEmitter<number> = new EventEmitter<number>();
  
  @ViewChild('alertaError', { static: false }) alertaError: ElementRef;
  @ViewChild('alertaNull',  { static: false }) alertaNull: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  
  ngOnChanges(changes: SimpleChanges) {
     if( this.tipoAlerta ==1){
          this.alertaNull.nativeElement.hidden=false;
     }
     if (this.tipoAlerta ==2){
          this.alertaError.nativeElement.hidden=false;
     }

  }
  
  dismisError(){
    this.alertaError.nativeElement.hidden=true;
    this.childAlert.emit();
  }

  dismisWarning(){
    this.alertaNull.nativeElement.hidden=true;
    this.childAlert.emit();
  }
  

}
