import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaArrayComponent } from './tabla-array.component';

describe('TablaArrayComponent', () => {
  let component: TablaArrayComponent;
  let fixture: ComponentFixture<TablaArrayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablaArrayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
