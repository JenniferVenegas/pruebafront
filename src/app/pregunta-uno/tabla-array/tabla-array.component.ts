import { Component, OnInit, Input } from '@angular/core';
import { INumero } from '../numero';


@Component({
  selector: 'app-tabla-array',
  templateUrl: './tabla-array.component.html',
  styleUrls: ['./tabla-array.component.css']
})
export class TablaArrayComponent implements OnInit {

  @Input() listaNumeros: INumero[];
  nombreColumnas = ['N\°', 'Number', 'Quantity', 'first position', 'last position'];


  constructor() { }

  ngOnInit(): void {
  }

}
