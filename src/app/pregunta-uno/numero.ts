
export interface INumero {
  number: number;
  quantity: number;
  first: number;
  last: number;
}
