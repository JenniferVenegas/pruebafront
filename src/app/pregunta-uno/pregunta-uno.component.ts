import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Injectable, Inject } from '@angular/core';
import { INumero } from './numero';
import { PreguntasService} from  '../services/preguntas.service';

@Component({
  selector: 'app-pregunta-uno',
  templateUrl: './pregunta-uno.component.html',
  styleUrls: ['./pregunta-uno.component.css']
})

@Injectable()
export class PreguntaUnoComponent implements OnInit {
  @ViewChild('Cajatexto', { static: false }) cajaTexto: ElementRef;
  @ViewChild('textbox', { static: false }) textbox: ElementRef;
  @ViewChild('spinnerCarga', { static: false }) spinnerCarga: ElementRef;
  @ViewChild('botonCargar', { static: false }) botonCargar: ElementRef;

  listaNumeros: Array<INumero>;
  retornoapi: any;
  tipoAlerta: number =0;
  childAlert = 0;

  constructor(private preguntasService: PreguntasService) {
  }

  ngOnInit(): void {
  }

  resetAlert(number: any):void {
    this.tipoAlerta =0;
  } 

  obtenerDatos(){
    this.activarButtonCarga();

    //consumimos servicio y actualizamos variable "retornoapi"
     this.preguntasService.obtenerArray()
     .subscribe(arrayDesdeApi=> {
                    this.retornoapi = arrayDesdeApi;
                },
              error => this.mostrarError(),
              () => this.AgruparDatos()); 
  }

  AgruparDatos(){
    this.resetButton();

    let listaNumeroAux:  Array<INumero> = [];
    //filtramos respuesta y guardamos arreglo de números en variable "retorno"
    var retorno : number[] = this.retornoapi['data'];

    if( retorno && retorno.length>0 ){
        //Si existe arreglo en retorno se recorre con ForEach. Cada nuevo valor se busca en "listaNumeroAux". 
        retorno.forEach(function(value, index){
              var NumFind = listaNumeroAux.find(n => n.number == value)
              // Si valor ya existe en arreglo se le suma 1 a quantity y se modifica última posición del número
              if(NumFind){
                  NumFind.quantity = NumFind.quantity +1;
                  NumFind.last = index;
              // Si valor no existe se crea nueva instancia de objeto y se le asigna index a primera y última posición del número.
              }else{
                  let aux : INumero = { number: value, quantity: 1, first: index, last: index };
                  listaNumeroAux.push(aux); 
              } 
        }); 
        //completada la tabla ordenamos los números retornados de menor a mayor
        this.OrdenarDatos(retorno);
    }else{
        this.mostrarWarning();
    }
    //actualizo variable que se enviará como Input a componente tabla-array
    this.listaNumeros = listaNumeroAux; 
  }


  OrdenarDatos(retorno :number[]){
      var salir = false;
      // borramos los números duplicados (no se especifica en el problema si mostrar o no duplicados)
      let retornoUnico = retorno.filter((el, index) => retorno.indexOf(el) === index)
      // se realiza ordenamiento burbuja al array sin los números duplicados. Si bien burbuja no es la mejor en cuanto a tiempo
      // otorga la ventaja de no iterar innecesariamente si el arreglo ya viene ordenado (o necesita pocos cambios)
      while (!salir) {
        salir = true;
        // iteramos en el array hasta ordenar todos los números (comparamos números de dos en dos)
        for (var i = 1; i < retornoUnico.length; i += 1) {
          if (retornoUnico[i - 1] > retornoUnico[i]) {
            salir = false;
            var tmp = retornoUnico[i - 1];
            retornoUnico[i - 1] = retornoUnico[i];
            retornoUnico[i] = tmp;
          }
        }
      }
      // Ya teniendo el arreglo ordenado se actualiza y muestra valor de textbox en vista
      this.cajaTexto.nativeElement.hidden=false;
      this.textbox.nativeElement.innerText = retornoUnico.toString();
  }


mostrarError(){
  this.resetButton()
  this.tipoAlerta=2;
}

mostrarWarning(){
  this.resetButton()
  this.tipoAlerta=1;
}

resetButton(){
  this.spinnerCarga.nativeElement.hidden=true;
  this.botonCargar.nativeElement.innerText = "Obtener datos diccionario";
  this.botonCargar.nativeElement.disabled = false;
}

activarButtonCarga(){
  this.spinnerCarga.nativeElement.hidden = false;
  this.botonCargar.nativeElement.innerText = "Cargando ..."
  this.botonCargar.nativeElement.disabled = true;
}

}
